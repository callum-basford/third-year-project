\contentsline {section}{\numberline {1}Introduction}{6}{section.1}
\contentsline {section}{\numberline {2}Project Timeline}{6}{section.2}
\contentsline {section}{\numberline {3}Approaches}{7}{section.3}
\contentsline {subsection}{\numberline {3.1}Frameworks}{7}{subsection.3.1}
\contentsline {section}{\numberline {4}Review of Technologies}{9}{section.4}
\contentsline {section}{\numberline {5}Similar Systems Analysis}{11}{section.5}
\contentsline {subsection}{\numberline {5.1}London Zoo}{11}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Marwell Zoo}{13}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Africa Alive!}{15}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Colchester Zoo}{17}{subsection.5.4}
\contentsline {section}{\numberline {6}User Stories}{19}{section.6}
\contentsline {subsection}{\numberline {6.1}Breaking down the stories}{20}{subsection.6.1}
\contentsline {section}{\numberline {7}Design Stage}{21}{section.7}
\contentsline {subsection}{\numberline {7.1}Object Oriented Analysis}{22}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Object Oriented Design}{22}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}Architectural Design}{22}{subsection.7.3}
\contentsline {subsection}{\numberline {7.4}Sequence Diagram}{23}{subsection.7.4}
\contentsline {subsection}{\numberline {7.5}State Diagram}{23}{subsection.7.5}
\contentsline {section}{\numberline {8}Implementation Stage}{23}{section.8}
\contentsline {subsection}{\numberline {8.1}Object Oriented Analysis}{23}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Object Oriented Design}{24}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}Architectural Design}{24}{subsection.8.3}
\contentsline {subsection}{\numberline {8.4}Sequence Diagram}{25}{subsection.8.4}
\contentsline {subsection}{\numberline {8.5}State Diagram}{25}{subsection.8.5}
\contentsline {section}{\numberline {9}Evaluation}{26}{section.9}
\contentsline {subsection}{\numberline {9.1}The App}{26}{subsection.9.1}
\contentsline {subsubsection}{\numberline {9.1.1}Home Page}{26}{subsubsection.9.1.1}
\contentsline {subsubsection}{\numberline {9.1.2}Menu Page}{28}{subsubsection.9.1.2}
\contentsline {subsubsection}{\numberline {9.1.3}Categories Page}{29}{subsubsection.9.1.3}
\contentsline {subsubsection}{\numberline {9.1.4}Animals Page}{29}{subsubsection.9.1.4}
\contentsline {subsubsection}{\numberline {9.1.5}Trail Page}{31}{subsubsection.9.1.5}
\contentsline {subsubsection}{\numberline {9.1.6}Animal Page}{32}{subsubsection.9.1.6}
\contentsline {subsubsection}{\numberline {9.1.7}Quizzes Page}{33}{subsubsection.9.1.7}
\contentsline {subsubsection}{\numberline {9.1.8}Quiz Page}{34}{subsubsection.9.1.8}
\contentsline {subsubsection}{\numberline {9.1.9}Question Page}{34}{subsubsection.9.1.9}
\contentsline {section}{\numberline {10}Conclusion}{37}{section.10}
\contentsline {section}{\numberline {11}Figures}{39}{section.11}
\contentsline {section}{References}{50}{section*.35}
